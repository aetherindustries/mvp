#! usr/bin/env python3
from __future__ import print_function

import sys
from TastyZebras import Node, Edge, Pathway
import numpy as np



def parseMetabalome(file = 'Scerivisea/data/reactions.dat'):
    '''
    Take in a metabolome file (reactions.dat), parse through it, and return a metabolome in the form of a dictionary
    key: name of chemical
    value: Node objects with input and output edges. edges contain list of strings for product and substrate

    :param file: metabolome file from metaCyc data example: reactions.dat
    :return: metabolome (dict)
    '''

    if not file:
        file = sys.stdin

    else:
        file = open(file)

    metabolome = {} #dictionary containing all nodes from metabolome

    #initialize containers for data we want to store
    ECnumber = list()
    substrates = list()
    products = list()
    scoeff = list()
    pcoeff = list()
    direction = ''
    allEdges = list()


    nextchar = ''

    for line in file:
        if not line.startswith('#'):

            line = nextchar+line
            nextchar = ''
            #print(line)

            line = line.strip().split(' - ')

            if line[0][:2] == '//':
                if ECnumber != list(): #nest these to prevent index error for value = line[1]
                    #create new node and clear old values

                    #replace compounds in reaction with our naming system

                    if len(products) != len(pcoeff):
                        print('ERROR', file=sys.stderr)
                    if len(substrates) != len(scoeff):
                        print('ERROR', file=sys.stderr)

                    direction, products, substrates, pcoeff, scoeff = _checkReactionDirection_(direction, products, substrates, pcoeff, scoeff)

                    for ec in ECnumber: #when reaction contains multiple EC numbers
                        newEdge = Edge(ec, direction, substrates, products, scoeff, pcoeff)
                        allEdges.append(newEdge)

                        for chemical in products: #build nodes out of products
                            if chemical not in metabolome.keys():
                                newNode = Node(chemical, inEnzyme=newEdge)
                                metabolome[chemical] = newNode
                            else:
                                oldNode = metabolome[chemical]
                                if newEdge not in oldNode.i:
                                    oldNode.addEdgeIn(newEdge)

                        for chemical in substrates: #build nodes out of substrates
                            if chemical not in metabolome.keys():
                                newNode = Node(chemical, outEnzyme=newEdge)
                                metabolome[chemical] = newNode
                            else:
                                oldNode = metabolome[chemical]
                                if newEdge not in oldNode.o:
                                    oldNode.addEdgeOut(newEdge)

                #clear data for next reaction from file
                ECnumber = list()
                products = list()
                substrates = list()
                scoeff = list()
                pcoeff = list()
                direction = ''

            else:
                attribute = line[0]

                value = line[1]

                if attribute == 'EC-NUMBER':
                    ec = value[3:]
                    ECnumber.append(ec)
                elif attribute == 'RIGHT':
                    products.append(value)
                    nextchar = file.read(4)
                    if nextchar == '^COE':
                        coefficient = file.readline().strip().split(' - ')[1]
                        try:
                            coefficient = int(coefficient)
                        except:
                            coefficient = coefficient.replace('n', '')
                            if coefficient is '':
                                coefficient = 1
                            else:
                                coefficient = int(coefficient)

                        pcoeff.append(coefficient)
                        nextchar = ''
                    else: pcoeff.append('1')

                elif attribute == 'LEFT':
                    substrates.append(value)
                    nextchar = file.read(4)
                    if nextchar == '^COE':
                        coefficient = file.readline().strip().split(' - ')[1]
                        try: coefficient = int(coefficient)
                        except:
                            coefficient = coefficient.replace('n', '')
                            if coefficient is '':
                                coefficient = 1
                            else: coefficient = int(coefficient)

                        scoeff.append(coefficient)
                        nextchar = ''
                    else: scoeff.append('1')
                elif attribute == 'REACTION-DIRECTION':
                    direction = value

    return metabolome, allEdges

def fillEdges(nodes):
    '''
    Fill product and substrate of edges in nodes in input dictionary with all node objects instead of strings.


    :param nodes: dictionary of all the nodes found in metabolome
    :return: metabolome
    '''

    for chemical, node in nodes.items():

        for edge in node.i:
            edge.product = [nodes[chem] if chem in nodes else chem for chem in edge.product]
            edge.substrate = [nodes[chem] if chem in nodes else chem for chem in edge.substrate]

        for edge in node.o:
            edge.product = [nodes[chem] if chem in nodes else chem for chem in edge.product]
            edge.substrate = [nodes[chem] if chem in nodes else chem for chem in edge.substrate]

    return nodes

def buildStoichiometricMatrix(metabolome, reactions):

    S = np.zeros([len(metabolome), len(reactions)], dtype=int)

    chemicals = np.array(sorted(metabolome.keys()))
    chemicalsIndex = {chem:i for i, chem in enumerate(chemicals)}

    reactionDirections = np.array([edge.direction for edge in reactions])

    for i, edge in enumerate(reactions):
        #looking at column S[:,i]

        for coefficient, substrate in zip(edge.substrateCoefficients, edge.substrate):
            chemIndex = chemicalsIndex[str(substrate)]
            S[chemIndex, i] = -int(coefficient)


        for coefficient, product in zip(edge.productCoefficients, edge.product):
            chemIndex = chemicalsIndex[str(product)]

            S[chemIndex, i] = int(coefficient)


    return S


def _checkReactionDirection_(direction, products, substrates, pcoeff, scoeff):
    if direction == 'LEFT-TO-RIGHT':
        direction = 'Unknown'
        return direction, products, substrates, pcoeff, scoeff

    elif direction == 'RIGHT-TO-LEFT':
        direction = 'Unkown'
        newp = substrates
        newpc = scoeff
        news = products
        newsc = pcoeff
        return direction, newp, news, newpc, newsc

    elif direction == 'PHYSIOL-LEFT-TO-RIGHT':
        direction = 'Unknown' #change to irrev?
        return direction, products, substrates, pcoeff, scoeff


    elif direction == 'PHYSIOL-RIGHT-TO-LEFT':
        direction = 'Unkown'
        newp = substrates
        newpc = scoeff
        news = products
        newsc = pcoeff
        return direction, newp, news, newpc, newsc

    elif direction == "IRREVERSIBLE-LEFT-TO-RIGHT":
        direction = 'IRREVERSIBLE'  # change to irrev?
        return direction, products, substrates, pcoeff, scoeff

    elif direction == 'IRREVERSIBLE-RIGHT-TO-LEFT':
        direction = 'IRREVERSIBLE'
        newp = substrates
        newpc = scoeff
        news = products
        newsc = pcoeff
        return direction, newp, news, newpc, newsc

    elif 'REVERSIBLE' in direction:
        direction = 'REVERSIBLE'
        return direction, products, substrates, pcoeff, scoeff

    else:
        direction = 'None'
        return direction, products, substrates, pcoeff, scoeff



def main(args):

    nodes, allEdges = parseMetabalome()       #parse the reactions.dat file

    metabolome = fillEdges(nodes)   #fill the edges with corresponding nodes

    #get stoichiometric matrix
    S = buildStoichiometricMatrix(metabolome, allEdges)


if __name__ == "__main__":
    sys.exit(main(sys.argv))