Dead-end metabolite report for YEAST on 2011-06-14_15-56-20

Parameters: (SMALL-MOLS-ONLY? NIL PATHWAYS-ONLY? NIL INCLUDE-COFACTORS?
             NIL IGNORE-UNKNOWN-DIRECTION? NIL COMPARTMENT CCO-CYTOSOL)

Reactants which are not products:

CPD-1302                                                    5-methyltetrahydropteroyltri-L-glutamate
CPD-558                                                     6-carboxyhexanoyl-CoA
CPD3O-0                                                     a protein acetyl-lysine
XYLOSE                                                      alpha-D-xylose
DEOXYCYTIDINE                                               deoxycytidine
CPD-7670                                                    dimethylsulfide
ETHANOL-AMINE                                               ethanol-amine
INOSITOL-PHOSPHATE                                          inositol phosphate
NICOTINAMIDE_RIBOSE                                         nicotinamide ribose
CPD-8259                                                    nicotinate riboside
SUPER-OXIDE                                                 O2-
PYRIDOXAL                                                   pyridoxal
PYRIDOXAMINE                                                pyridoxamine
PYRIDOXINE                                                  pyridoxine
CPD1G-159                                                   RX
SUCROSE                                                     sucrose
THYMINE                                                     thymine

Products which are not reactants:

1-AMINO-PROPAN-2-OL                                         1-amino-propan-2-ol
RIBITYLAMINO-AMINO-DIHYDROXY-PYR                            4-(1-D-ribitylamino)-5-amino-2,6-dihydroxypyrimidine
ACETYL-ADP-RIBOSE                                           acetyl-ADP-ribose
CPD-1789                                                    dehydro-D-arabinono-1,4-lactone
DATP                                                        deoxy-ATP
DGTP                                                        deoxy-GTP
2-DEOXYRIBOSE                                               deoxyribose
TTP                                                         dTTP
ERGOSTEROL                                                  ergosterol
CPD3O-410                                                   Glc(3)Man(9)GlcNAc(2)-PP-Dol
GLUTARATE                                                   glutarate
GLYCOGENN-1                                                 Glycogen(N-1)
GLYCOLALDEHYDE                                              glycolaldehyde
CPD1G-160                                                   HX
CPD-341                                                     indole-3-ethanol
MALTOSE                                                     maltose
MIP2C                                                       MIP2C
PALMITALDEHYDE                                              palmitaldehyde
CPD-9245                                                    palmitoleate
PHENYLETHANOL                                               phenylethanol
PHOSPHATIDYLCHOLINE-CMPD                                    phosphatidylcholine
PROPIONATE                                                  propionate
PYRIDOXAL_PHOSPHATE                                         pyridoxal 5'-phosphate
S-ADENOSYL-4-METHYLTHIO-2-OXOBUTANOATE                      S-adenosyl-4-methylthio-2-oxobutanoate
SIROHEME                                                    siroheme
TRIMETHYLSULFONIUM                                          trimethyl sulfonium
CPD3O-4151                                                  tyrosol