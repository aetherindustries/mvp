
====Recomputing statistics for database yeastcyc====

 Recomputed statistics are as follows:
(PSEUDOGENE-NUMBER 0 ENZYMATIC-REACTION-NUMBER 981 CITATION-COUNT 864
 LAST-TIME-STAMP 01-Jun-2011  16:09:03 TRNA-NUMBER 0 OPERON-NUMBER 1
 TOTAL-GENE-NUMBER 811 CPD-NUMBER 688 TRANSPORTER-NUMBER 0
 ENZYME-NUMBER 708 COMPLEX-NUMBER 48 POLYPEPTIDE-NUMBER 826
 TRANSPORT-REACTION-NUMBER 2 ORGANISM-REACTION-NUMBER 993
 NO-EC-REACTION-NUMBER 202 REACTION-NUMBER 993 ORF-GENE-NUMBER 809
 RNA-GENE-NUMBER 0 PROTEIN-GENE-NUMBER 811 UN-MAPPED-GENE-NUMBER 0
 MAPPED-GENE-NUMBER 811 GC-CONTENT 0 GENOME-LENGTH 12156306)

== Done recomputing statistics for database yeastcyc ==
The report from this consistency checker run can be found at
/usr/local/ptools-local/pgdbs/user/yeastcyc/15.0/reports/consistency-checker-report-2011-06-01_16-09-02.txt
