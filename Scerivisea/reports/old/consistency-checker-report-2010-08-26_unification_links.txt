Replacing link (SGD S000000755) with (SGD "S000000755") in frame G3O-10.
Replacing link (SGD S000006322) with (SGD "S000006322") in frame G3O-111.
Replacing link (SGD S000004007) with (SGD "S000004007") in frame G3O-114.
Replacing link (SGD S000003785) with (SGD "S000003785") in frame G3O-132.
Replacing link (SGD S000000764) with (SGD "S000000764") in frame G3O-137.
Replacing link (SGD S000004611) with (SGD "S000004611") in frame G3O-142.
Replacing link (SGD S000002505) with (SGD "S000002505") in frame G3O-1594.
Replacing link (SGD S000000976) with (SGD "S000000976") in frame G3O-1601.
Replacing link (SGD S000005980) with (SGD "S000005980") in frame G3O-1606.
Replacing link (SGD S000003441) with (SGD "S000003441") in frame G3O-19.
Replacing link (SGD S000002580) with (SGD "S000002580") in frame G3O-24.
Replacing link (SGD S000004644) with (SGD "S000004644") in frame G3O-31.
Replacing link (SGD S000002424) with (SGD "S000002424") in frame G3O-32.
Replacing link (SGD S000003636) with (SGD "S000003636") in frame G3O-343.
Replacing link (SGD S000000679) with (SGD "S000000679") in frame G3O-35.
Replacing link (SGD S000004296) with (SGD "S000004296") in frame G3O-356.
Replacing link (SGD S000004402) with (SGD "S000004402") in frame G3O-37.
Replacing link (SGD S000002695) with (SGD "S000002695") in frame G3O-4.
Replacing link (SGD S000005073 NIL NIL NIL NIL NIL) with (SGD
                                                          "S000005073"
                                                          NIL
                                                          NIL
                                                          NIL
                                                          NIL
                                                          NIL) in frame G3O-4113.
Replacing link (SGD S000001695) with (SGD "S000001695") in frame G3O-60.
Replacing link (SGD S000005050) with (SGD "S000005050") in frame G3O-65.
Replacing link (SGD S000004033) with (SGD "S000004033") in frame G3O-7.
Replacing link (SGD S000005635) with (SGD "S000005635") in frame G3O-70.
Replacing link (SGD S000003871) with (SGD "S000003871") in frame G3O-75.
Replacing link (SGD S000001264) with (SGD "S000001264") in frame G3O-80.
Replacing link (SGD S000004902) with (SGD "S000004902") in frame G3O-84.
Replacing link (SGD S000005426) with (SGD "S000005426") in frame G3O-85.
Replacing link (SGD S000005269) with (SGD "S000005269") in frame G3O-91.
Replacing link (SGD S000001050) with (SGD "S000001050") in frame YHR008C.
Replacing link (SGD S000003865) with (SGD "S000003865") in frame YJR104C.
Replacing link (SGD S000004913) with (SGD "S000004913") in frame YMR298W.
The following frames have unification links to object '17210-42-3' in database CAS: (RIBOSE-1P
                                                                                     DEOXY-RIBOSE-1P)
The following frames have unification links to object '548-93-6' in database CAS: (ANTHRANILATE
                                                                                   3-HYDROXY-ANTHRANILATE)
The following frames have unification links to object 'C00390' in database LIGAND-CPD: (UBIQUINOL-30
                                                                                        QH2)
The following frames have unification links to object 'C00262' in database LIGAND: (HYPOXANTHINE
                                                                                    XANTHINE)
The following frames have unification links to object 'C00198' in database LIGAND-CPD: (CPD-8549
                                                                                        GLC-D-LACTONE)
The following frames have unification links to object '1173-82-6' in database CAS: (UTP
                                                                                    DUTP)
The following frames have unification links to object '6816' in database PUBCHEM: (CO-A
                                                                                   P-COUMAROYL-COA)
The following frames have unification links to object 'C00010' in database LIGAND-CPD: (CO-A
                                                                                        P-COUMAROYL-COA)
The following frames have unification links to object '403664' in database NCI: (CPD-249
                                                                                 Sulfurated-Sulfur-Acceptors)
The following frames have unification links to object 'C01268' in database LIGAND-CPD: (AMINO-RIBOSYLAMINO-1H-3H-PYR-DIONE-P
                                                                                        CPD-602)
The following frames have unification links to object '9007-43-6' in database CAS: (CPD-2363
                                                                                    CPD-2361)
The following frames have unification links to object '964-26-1' in database CAS: (UMP
                                                                                   DUMP)
The following frames have unification links to object '7704-34-9' in database CAS: (CPD-249
                                                                                    Sulfurated-Sulfur-Acceptors)
The following frames have unification links to object '559292' in database NCBI-TAXONOMY-DB: (TAX-559292
                                                                                              YEAST)
The following frames have unification links to object 'C03722' in database LIGAND-CPD: (QUINOLINIC_ACID
                                                                                        QUINOLINATE)
The following frames have unification links to object 'S000001264' in database SGD: (YIL002C
                                                                                     G3O-80)
The following frames have unification links to object 'C00332' in database LIGAND-CPD: (ACETOACETYL-COA
                                                                                        ACETYL-COA)
The following frames have unification links to object 'C04230' in database LIGAND-CPD: (L-1-LYSOLECITHIN
                                                                                        2-ACYLGLYCEROPHOSPHOCHOLINE)
The following frames have unification links to object '97279-79-3' in database CAS: (CHORISMATE
                                                                                     4-AMINO-4-DEOXYCHORISMATE)
The following frames have unification links to object 'S000005050' in database SGD: (YNL106C
                                                                                     G3O-65)
The following frames have unification links to object '53-84-9' in database CAS: (NADH
                                                                                  NAD)
The following frames have unification links to object '17896' in database CHEBI: (L-1-LYSOLECITHIN
                                                                                  2-ACYLGLYCEROPHOSPHOCHOLINE)
The following frames have unification links to object '979-92-0' in database CAS: (HOMO-CYS
                                                                                   ADENOSYL-HOMO-CYS
                                                                                   CYS)
The following frames have unification links to object 'Caldesmon' in database Wikipedia: (Phosphorylated-Caldesmon-Proteins
                                                                                          Caldesmon-Proteins)
The following frames have unification links to object '17510-99-5' in database CAS: (GLUCONATE
                                                                                     2-DEHYDRO-3-DEOXY-D-GLUCONATE)
The following frames have unification links to object '134-35-0' in database CAS: (THF
                                                                                   5-METHYL-THF)
The following frames have unification links to object '5280344' in database PUBCHEM: (UBIQUINOL-30
                                                                                      QH2)
The following frames have unification links to object 'C00092' in database LIGAND-CPD: (D-glucose-6-phosphate
                                                                                        GLC-6-P)
The following frames have unification links to object '104809-02-1' in database CAS: (METHYL-MALONYL-COA
                                                                                      D-METHYL-MALONYL-COA)
The following frames have unification links to object '53-59-8' in database CAS: (NADP
                                                                                  ADP)
The following frames have unification links to object '482-67-7' in database CAS: (PAPS
                                                                                   APS)
The following frames have unification links to object 'C04454' in database LIGAND-CPD: (AMINO-DIOXY-PHOSPHORIBITYLAMINO-PYR
                                                                                        CPD-1086)
The following frames have unification links to object '1420-36-6' in database CAS: (ACETOACETYL-COA
                                                                                    ACETYL-COA)
The following frames have unification links to object '1054' in database PUBCHEM: (PYRIDOXINE-5P
                                                                                   PYRIDOXINE)
The following frames have unification links to object '16709' in database CHEBI: (PYRIDOXINE-5P
                                                                                  PYRIDOXINE)
The following frames have unification links to object '15346' in database CHEBI: (CO-A
                                                                                  P-COUMAROYL-COA)
The following frames have unification links to object '440751' in database PUBCHEM: (CPD-340
                                                                                     HOMO-I-CIT)
The following frames have unification links to object '102916-66-5' in database CAS: (RIBOSE-5P
                                                                                      DEOXY-RIBOSE-5P)
The following frames have unification links to object 'S000005635' in database SGD: (YOR109W
                                                                                     G3O-70)
The following frames have unification links to object '30904' in database CHEBI: (CPD-340
                                                                                  HOMO-I-CIT)
The following frames have unification links to object '16675' in database CHEBI: (QUINOLINIC_ACID
                                                                                  QUINOLINATE)
The following frames have unification links to object '146-14-5' in database CAS: (FARNESYL-PP
                                                                                   FAD)
The following frames have unification links to object '20762-30-5' in database CAS: (RIBOSE
                                                                                     ADENOSINE_DIPHOSPHATE_RIBOSE)
The following frames have unification links to object 'S000005426' in database SGD: (YOL065C
                                                                                     G3O-85)
The following frames have unification links to object '2922-42-1' in database CAS: (SHIKIMATE
                                                                                    3-DEHYDRO-SHIKIMATE)
The following frames have unification links to object 'C05662' in database LIGAND-CPD: (CPD-340
                                                                                        HOMO-I-CIT)
The following frames have unification links to object 'C00314' in database LIGAND-CPD: (PYRIDOXINE-5P
                                                                                        PYRIDOXINE)
The following frames have unification links to object '17976' in database CHEBI: (UBIQUINOL-30
                                                                                  QH2)
The following frames have unification links to object '102029-88-9' in database CAS: (N-ACETYL-D-GLUCOSAMINE-6-P
                                                                                      D-GLUCOSAMINE-6-P)
The following frames have unification links to object '68-94-0' in database CAS: (HYPOXANTHINE
                                                                                  XANTHINE)
The following frames have unification links to object '65207-12-7' in database CAS: (DIHYDROSIROHYDROCHLORIN
                                                                                     SIROHYDROCHLORIN)
The following frames have unification links to object '1066' in database PUBCHEM: (QUINOLINIC_ACID
                                                                                   QUINOLINATE)
The report from this consistency checker run can be found at
/usr/local/ptools-local/pgdbs/user/yeastcyc/14.0/reports/consistency-checker-report-2010-08-26_14-38-15.txt
