
===== Checking pathway predecessors/reactions/subs ====

Removing nonexistent pwy from super-pathway: BRANCHED-CHAIN-AA-SYN-PWY
Removing nonexistent pwy from super-pathway: P4-PWY
Removing nonexistent pwy from super-pathway: PWY-821
Removing nonexistent pwy from super-pathway: YEAST-ARG-SYN-PWY
Removing nonexistent pwy from super-pathway: PANTOSYN2-PWY
Removing nonexistent pwy from super-pathway: NADSYN-PWY
Removing nonexistent pwy from super-pathway: PWY3O-4158
PWY3O-862 has sub-pathways but is not itself a child of Super-Pathways; it
will now be added as a child of Super-Pathways
HOMOCYS-CYS-CONVERT has sub-pathways but is not itself a child of Super-Pathways; it
will now be added as a child of Super-Pathways
GLUTSYN-PWY has sub-pathways but is not itself a child of Super-Pathways; it
will now be added as a child of Super-Pathways
PANTOSYN2-PWY has sub-pathways but is not itself a child of Super-Pathways; it
will now be added as a child of Super-Pathways
ARGDEG-YEAST-PWY has sub-pathways but is not itself a child of Super-Pathways; it
will now be added as a child of Super-Pathways

===== Removing newlines from names ====


(YJL060W-MONOMER "Kynurenine aminotransferase
") 
(|Reduced-Factor-F420| "reduced cofactor F<sub>420</sub> ") 
(|D-Galactosyl-12-diacyl-glycerols| "a 

monogalactosyldiglyceride") 
(|Phosphatidates-36-4| "phosphatidate-36:4	") 
===== Clearing read-only computed slot values ====


== Done running all the Misc automatic checks ==
The report from this consistency checker run can be found at
/usr/local/ptools-local/pgdbs/user/yeastcyc/14.0/reports/consistency-checker-report-2010-08-26_15-32-20.txt
