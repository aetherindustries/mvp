
==== Reaction balance summary report for yeastcyc ====

TOTAL BALANCED REACTIONS: 886
    With :CANNOT-BALANCE? slot set to TRUE: 1
TOTAL UNBALANCED REACTIONS: 39
    With :CANNOT-BALANCE? slot set to TRUE: 37
    With :CANNOT-BALANCE? slot not set: 2
TOTAL UNDETERMINED REACTIONS: 19
    With one or more of the substrates lack a chemical structure: 15
    With non-numerical coefficients: 4
======================================================


=== The following reactions are BALANCED but their :CANNOT-BALANCE? slot is set to TRUE. When a reaction is BALANCED, the :CANNOT-BALANCE? slot must be set to NIL. Please review these reactions:

2.3.1.42-RXN:                                a long-chain acyl-CoA + dihydroxyacetone phosphate  ->  an acylglycerone phosphate + coenzyme A

=== The following 2 reactions are unbalanced and do not have the :CANNOT-BALANCE? slot set to TRUE:


PROTEIN-TYROSINE-PHOSPHATASE-RXN:            a protein-L-tyrosine phosphate + H2O  =  a [protein]-L-tyrosine + a [protein]-L-tyrosine + phosphate
RXN3O-214:                                   beta-D-ribosylnicotinate + H2O  ->  D-ribose + nicotinate

=== The following reactions have balanced state UNDETERMINED:
  A balance state of UNDETERMINED means that the software cannot determine whether
the reaction is balanced, such as because reaction substrates lack chemical structures.

3.2.1.33-RXN:                                a limit dextrin with short branches + H2O  =  a debranched limit dextrin + beta-D-glucose
RXN-9023:                                    a limit dextrin  =  a limit dextrin with short branches
FATTY-ACID-SYNTHASE-RXN:                     acetyl-CoA + n malonyl-CoA + 2n NADPH + 2n H+  =  a long-chain fatty acid + n CO2 + (n+1) coenzyme A + 2n NADP+
RXN-1826:                                    [(1->4)-alpha-D-glucosyl]n + phosphate  =  [(1->4)-alpha-D-glucosyl](n-1) + alpha-D-glucose 1-phosphate
RXN-9025:                                    a debranched limit dextrin + n phosphate  =  n alpha-D-glucose 1-phosphate
RXN0-6941:                                   2 Fe2+ + 2 (2,3-dihydroxybenzoylserine)3 + NADP+  <-  2 ferric (2,3-dihydroxybenzoylserine)3 + NADPH + H+
RXN-9137:                                    isopentenyl diphosphate + di-trans-(cis-polyprenyl)n-diphosphate  =  di-trans-(cis-polyprenyl)n+1-diphosphate + diphosphate
NQOR-RXN:                                    a quinone + NAD(P)H + H+  =  a quinol + NAD(P)+
RXN-12000:                                   apo-4'-lycopenal + NAD(P)+ + H2O  =  apo-4'-lycopenoate + NADPH + H+
PEROXID-RXN:                                 a phenolic donor + hydrogen peroxide  =  2 a phenoxyl radical of a phenolic donor + 2 H2O
RXN0-5146:                                   a limit dextrin + H2O  ->  maltotetraose + a debranched limit dextrin
1.11.1.12-RXN:                               a lipid hydroperoxide + 2 glutathione  ->  a lipid + glutathione disulfide + 2 H2O
3.2.1.14-RXN:                                chitin + n H2O  =  n a chitodextrin
CERAMIDASE-YEAST-RXN:                        an alpha hydroxyphytoceramide + H2O    phytosphingosine + a hydroxy fatty acid
CHITIN-DEACETYLASE-RXN:                      chitin + n H2O  ->  chitosan + n acetate
FATTY-ACYL-COA-SYNTHASE-RXN:                 acetyl-CoA + n malonyl-CoA + 2n NADH + 2n NADPH + 4n H+  =  a long-chain acyl-CoA + n CO2 + n coenzyme A + 2n NAD+ + 2n NADP+
GLYCOPHOSPHORYL-RXN:                         a glycogen + phosphate  ->  a limit dextrin + alpha-D-glucose 1-phosphate
GSHTRAN-RXN:                                 glutathione + RX  =  a glutathione-toxin conjugate + HX
QOR-RXN:                                     a quinone + NADPH  =  a semiquinone + NADP+

The report from this consistency checker run can be found at
p:\aic\pgdbs\tier1\yeastcyc\16.5\reports\consistency-checker-report-2012-10-19_15-52-30.txt
