
=====Checking all inverse links====

[Checking inverses under class  Regulation]
[Checking inverses under class  Publications]
[Checking inverses under class  Protein-Features]
[Checking inverses under class  Polymer-Segments]
[Checking inverses under class  People]
[Checking inverses under class  Paralogous-Gene-Groups]
[Checking inverses under class  Organizations]
[Checking inverses under class  Organisms]
[Checking inverses under class  Notes]
[Checking inverses under class  KB-DESCRIPTOR]
[Checking inverses under class  Growth-Observations]
[Checking inverses under class  Generalized-Reactions]
[Checking inverses under class  Gene-Ontology-Terms]
[Checking inverses under class  Evidence]
[Checking inverses under class  Enzymatic-Reactions]
[Checking inverses under class  EC-BOOKKEEPING]
[Checking inverses under class  Databases]
[Checking inverses under class  Compound-Mixtures]
[Checking inverses under class  Chemicals]
[Checking inverses under class  CCO]

===== Checking and removing any values from PATHWAY-LINKS that point to nonexistent frames ====


===== Checking and removing any reactions from PRIMARIES slot that are no longer in the pathway ====


===== Checking modified protein links to eliminate nonexistent frames ====


===== Cross-referencing compounds and reactions ====


==== Calculating sub and super pathways ====

===== Finding missing sub pathway links ====


==== Checking for missing sub pathway links ====
Warning:PWY-6543 is completely contained within PWY3O-1874 but is not listed in the SUB-PATHWAYS slot

Warning:PWY3O-1109 is completely contained within PWY-5754 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-6164 is completely contained within ARO-YEAST-PWY but is not listed in the SUB-PATHWAYS slot

Warning:PWY-6293 is completely contained within PWY-821 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-5486 is completely contained within PWY-6333 but is not listed in the SUB-PATHWAYS slot

Warning:UDPNAGSYN-YEAST-PWY is completely contained within PWY-6981 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-5123 is completely contained within HEXPPSYN-PWY-1 but is not listed in the SUB-PATHWAYS slot

Warning:PWY3O-50 is completely contained within PWY-5920 but is not listed in the SUB-PATHWAYS slot

Warning:PWYQT-4432 is completely contained within PWY-4041 but is not listed in the SUB-PATHWAYS slot

Warning:PWY3O-1874 is completely contained within PWY-6543 but is not listed in the SUB-PATHWAYS slot

Warning:PWY66-385 is completely contained within PWY3O-697 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-841 is completely contained within DENOVOPURINE3-PWY but is not listed in the SUB-PATHWAYS slot

Warning:PWY-6536 is completely contained within GLUDEG-I-PWY-1 but is not listed in the SUB-PATHWAYS slot

Warning:PWY3O-4109 is completely contained within PWY-5078 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-5078 is completely contained within PWY3O-4109 but is not listed in the SUB-PATHWAYS slot

Warning:PWY-5486 is completely contained within PWY-7118 but is not listed in the SUB-PATHWAYS slot

Warning:GLYCOLYSIS-YEAST-PWY is completely contained within ANAGLYCOLYSIS-PWY but is not listed in the SUB-PATHWAYS slot


==Done checking all the links==
The report from this consistency checker run can be found at
p:\aic\pgdbs\tier1\yeastcyc\16.5\reports\consistency-checker-report-2012-10-19_14-27-06.txt
