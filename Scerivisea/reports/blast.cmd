Executable   = /homedir/green/cluster/blastall.$$(OpSys).$$(Arch)
Universe     = vanilla
Log          = RXN-8773.condor-log
Arguments    = -e 1.0 -p blastp -m 7  -d eq.fsa
Transfer_input_files = /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa, /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa.phr, /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa.pin, /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa.psd, /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa.psi, /homedir/brg/aic/pgdbs/tier1/yeastcyc/16.6/data/protseq.fsa.psq, /home/hapuna2/bio/software/blast-linux/data/BLOSUM62
Should_transfer_files = YES
When_to_transfer_output = ON_EXIT
Notification = Never
Requirements = (Memory > 200) && (Disk >= DiskUsage) && (HasFileTransfer) && ((Arch == "INTEL") || (Arch == "SUN4u") || (Arch == "x86_64")) && ((OpSys == "LINUX") || (OpSys == "SOLARIS28"))
Queue 1
