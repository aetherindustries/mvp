
;;; ---  PGDB Incremental Update ---
;;; ---  Parse Results for Saccharomyces cerevisiae  ---
;;;
;;; This report summarizes the findings of the PathoLogic parser for
;;; the annotation files for all updated genetic elements.  Through
;;; inspection of this report, the user can tell whether features of major 
;;; importance were not recognized, possibly due to syntax errors or other
;;; problems in the updated annotation files. If you want to change 
;;; annotation file(s) to improve their parsability, update the files 
;;  then re-run the Update.


;;; -----------------------------------------------------------

;;; Parsing results for annotation file:
;;;        chr01.pf

;;;
;;; The annotation file used by the parser is:
;;; chr01.pf
gene-IDs :                       102
gene-name :                      102
gene-startbase :                 102
gene-endbase :                   102
gene-product-type :              102
  gene-Proteins :                   92
  gene-tRNAs :                       4
  gene-rRNAs :                       0
  gene-snRNAs :                      0
  gene-miscRNAs :                    4
product-IDs :                      0 [cannot be supplied in Genbank files]
product-names :                   88
product-synonyms :                 0 [cannot be supplied in Genbank files]
total EC numbers :                19
  genes with an EC number :         19
  genes with >1 EC numbers :         0
total supplied MetaCyc reaction ids :       0
  genes with a supplied MetaCyc reaction id :       0
  genes with >1 supplied MetaCyc reaction id :       0
total GO terms linked to reactions :       0
  genes with a GO term linked to reactions :       0
  genes with >1 GO terms linked to reactions :       0
gene-products:
  hypothetical :                     0
  predicted :                        0
  putative :                         0
genes with synonyms :              0

;;; -----------------------------------------------------------

;;; Parsing results for annotation file:
;;;        chr02.pf

;;;
;;; The annotation file used by the parser is:
;;; chr02.pf
gene-IDs :                       427
gene-name :                      427
gene-startbase :                 427
gene-endbase :                   427
gene-product-type :              427
  gene-Proteins :                  402
  gene-tRNAs :                      13
  gene-rRNAs :                       0
  gene-snRNAs :                      0
  gene-miscRNAs :                   12
product-IDs :                      0 [cannot be supplied in Genbank files]
product-names :                  382
product-synonyms :                 0 [cannot be supplied in Genbank files]
total EC numbers :               115
  genes with an EC number :        115
  genes with >1 EC numbers :         0
total supplied MetaCyc reaction ids :       0
  genes with a supplied MetaCyc reaction id :       0
  genes with >1 supplied MetaCyc reaction id :       0
total GO terms linked to reactions :       0
  genes with a GO term linked to reactions :       0
  genes with >1 GO terms linked to reactions :       0
gene-products:
  hypothetical :                     0
  predicted :                        0
  putative :                         0
genes with synonyms :              0

;;; -----------------------------------------------------------

;;; Parsing results for annotation file:
;;;        chr03.pf

;;;
;;; The annotation file used by the parser is:
;;; chr03.pf
gene-IDs :                       178
gene-name :                      178
gene-startbase :                 178
gene-endbase :                   178
gene-product-type :              178
  gene-Proteins :                  159
  gene-tRNAs :                      10
  gene-rRNAs :                       0
  gene-snRNAs :                      0
  gene-miscRNAs :                    7
product-IDs :                      0 [cannot be supplied in Genbank files]
product-names :                  163
product-synonyms :                 0 [cannot be supplied in Genbank files]
total EC numbers :                35
  genes with an EC number :         35
  g