#! usr/bin/env python3

class Node:
    '''
    Object to model a chemical in a metabolic network

    methods:
        currently used:
            addEdgeIn: add an input edge to the Node
            addEdgeOut: add an output edge to the Node

        not currently used:
            getInput: get all nodes that go into this node
            getOutput: get all nodes that this node goes into
    '''

    def __init__(self, chemical, inEnzyme = None, outEnzyme = None):
        '''
        Construct object with relevant attributes described with inline comments

        :param chemical: name of chemical this node is representing
        :param inEnzyme: An input Edge with corresponding substrates and products referencing other nodes
        :param outEnzyme: An output Edge with corresponding substrates and products referencing other nodes
        '''

        self.chemical = chemical #name of chemical/node

        if inEnzyme:
            self.i = [inEnzyme] #list of possible input enzymes or Ec number
        else:
            self.i = list()

        if outEnzyme:
            self.o = [outEnzyme] #list of possible output enzymes or Ec number
        else: self.o = list()

        self.protein = None #

        #distinguish if in metabolome or newly generated, will be switched to boolean when necessary
        self.inMetabalome = None
        self.newNode = None

    def addEdgeIn(self, edge):
        '''
        Add an input edge to the Node
        :param edge: Edge object to be added to Node
        '''
        self.i.append(edge)

    def addEdgeOut(self, edge):
        '''
        Add an output edge to the Node
        :param edge:
        '''
        self.o.append(edge)

    def getOutput(self):
        '''
        :return: list of products for this edge
        '''
        output = [edge.product for edge in self.o]
        return output

    def getInput(self):
        '''
        :return: list of substrates for this edge
        '''
        input = [edge for edge in self.i]
        return input

    def __str__(self):
        return self.chemical


class Edge:
    '''
    Object to model an Edge, a connection between Nodes in a metabolic network

    methods:
        No current methods besides __init__ and built in str/eq
    '''

    def __init__(self, ECnumber, direction, substrate, product, substrateCoefficients, productCoefficients):
        '''
        Construct an Edge object with relevant attributes
        :param ECnumber: Ec Number of reaction catalyzed
        :param substrate: substrate(s)
        :param product: product(s)
        :param substrateCoefficients: list of the coefficients for substrate(s)
        :param productCoefficients: list of the coefficients for product(s)
        '''

        self.ECnumber = ECnumber

        self.substrate = substrate #input substrate(s)
        self.product = product #output product(s)

        self.substrateCoefficients = substrateCoefficients
        self.productCoefficients = productCoefficients

        self.direction = direction #can be set if non reversible reaction

        if len(product)!=len(productCoefficients) or len(substrate)!=len(substrateCoefficients):
            print('ERROR: incorrect number of coefficients on ')

    def __str__(self):
        return self.ECnumber

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


    def printMe(self):
        print(' + '.join([coeff + " " + str(chem) for coeff, chem in zip(self.substrateCoefficients, self.substrate)]))
        print('-->')
        print(' + '.join([coeff+" "+str(chem) for coeff, chem in zip(self.productCoefficients,self.product )]))



class Pathway:

    def __init__(self, start, end):

        self.startNode = start
        self.endNode = end


class Network:

    def __init__(self):
        pass